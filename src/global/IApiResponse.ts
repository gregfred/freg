export interface IApiResponse {
    message?:string, 
    errors?:string[], 
    content?:any, 
}