import * as React from 'react';
import * as ReactRouter from "react-router-dom";
import * as MobX from 'mobx-react';
import Notification from 'react-web-notification'

import {UiStore} from'../stores/UiStore'

class BrowserNotification extends React.Component <any, any> {
    readonly  uiStore : UiStore;

    constructor(props: any) {
        super(props);

        this.uiStore  = this.props.storesInstance.uiStore;
    }

    handlePermissionGranted(){
        console.log('Permission Granted');
        this.uiStore.ignoreBrowserNotifications = false;
      }
      handlePermissionDenied(){
        console.log('Permission Denied');
        this.uiStore.ignoreBrowserNotifications = true;
      }
      handleNotSupported(){
        console.log('Web Notification not Supported');
        this.uiStore.ignoreBrowserNotifications = true;
      }
    
      handleNotificationOnClick(e, tag){
        console.log(e, 'Notification clicked tag:' + tag);
        this.props.history.push("/chat")
      }
    
      handleNotificationOnError(e, tag){
        console.log(e, 'Notification error tag:' + tag);
      }
    
      handleNotificationOnClose(e, tag){
      }
    
      handleNotificationOnShow(e, tag){
      }

      handleButtonClick() {
        const title = 'React-Web-Notification' ;
        const body = 'Hello' + new Date();
        const tag = 'taggy';

        this.uiStore.triggerBrowserNotification(title, body, tag);

      }      

    render() {     
        return (

            <React.Fragment>               
                {false && 
                <Notification
                    ignore={this.uiStore.ignoreBrowserNotifications && this.uiStore.browserNotificationTitle !== ''}
                    notSupported={this.handleNotSupported.bind(this)}
                    onPermissionGranted={this.handlePermissionGranted.bind(this)}
                    onPermissionDenied={this.handlePermissionDenied.bind(this)}
                    onShow={this.handleNotificationOnShow.bind(this)}
                    onClick={this.handleNotificationOnClick.bind(this)}
                    onClose={this.handleNotificationOnClose.bind(this)}
                    onError={this.handleNotificationOnError.bind(this)}
                    timeout={5000}
                    title={this.uiStore.browserNotificationTitle}
                    options={this.uiStore.browserNotificationOptions}
                    //swRegistration={this.props.swRegistration}
                />
                }          
            </React.Fragment>
        )
    }
}

export default MobX.inject('storesInstance')(ReactRouter.withRouter(MobX.observer(BrowserNotification)))